/*
 * useful.js
 * https://github.com/raklaptudirm/useful.js
 *
 * Copyright (c) 2021 Rak Laptudirm
 * Licensed under the MIT license.
 */

/*
 * parseArgs.js
 * 
 * Parse commandline arguments from
 * process.argv.
 */

module.exports = function (args) {
    args = args.slice(2)
    args.command = args.splice(0, 1)[0]
    args.subcommands = []
    args.flags = []

    let flag = false
    let flagVal = {
      name: "",
      args: [],
    }

    for (const i of args) {
      if (i.startsWith("-")) {
        if (flag) {
          args.flags.push(flagVal)
        }
        flagVal = {
          name: i,
          args: [],
        }
      } else {
        if (flag) {
          flagVal.args.push(i)
        } else {
          args.subcommands.push(i)
        }
      }
    }

    if (flag) {
      args.flags.push(flagVal)
    }
    return args
  }